# General process for Security engineer in security releases

The main difference is in who initiates the release process. 
* **[Critical](#critical-security-releases)** security releases are initiated on-demand by the security team to resolve the most severe vulnerabilities that are labelled as `S1`. They are highly urgent and need to be released as soon as the patch is ready.
* **[Regular](#regular-security-releases)** security releases are scheduled monthly and are initiated by the release management team. 

## Dependencies updates
Fixes that mitigate 3rd party vulnerabilities are typically added at the end of the blog post with a reference to the existing CVE ID that was assigned to that vulnerability by the 3rd party.

[Example of previously used wording](https://about.gitlab.com/releases/2020/03/26/security-release-12-dot-9-dot-1-released/):
```
### Update Nokogiri dependency
The Nokogiri dependency has been upgraded to 1.10.8. This upgrade include a security fix for [CVE-2020-7595](https://github.com/advisories/GHSA-7553-jr98-vx47).
```

## Overall process

1. [Prepare blog post](#prepare-blog-post)
2. Follow the appropriate sub-process for a [critical](#critical-security-releases) or [regular](#regular-security-releases) security release
3. [Verify fixes](#verify-fixes)
4. [Finalize the release](#finalize-release)
6. [Post release](post-release). Once completed, you have finished the security release.

### Prepare blog post

* :warning: The developer has provided the range of affected versions: ask on the related security issues to provide it if not already provided.
    * This is required to request [CVEs](#requesting-cves). See also [GitLab's CVE Numbering Authority page](https://about.gitlab.com/security/cve/) as well as the instructions below.
* Create a WIP blog post MR in dev.gitlab.org:
  1. Ensure to add the CVE IDs, vulnerability descriptions, affected versions, remediation info, and a thank you to the reporter. 
       * Much of this information can be copy and pasted from the [summary table] in the developer security issue created on [GitLab Security]. 
  1. Feel free to use a past security release as a guide, for example [this blog post](https://about.gitlab.com/releases/2020/07/01/security-release-13-1-2-release/).
* Please add `/source/images/blogimages/security-cover-new.png` to the `image_title:` field in the front matter.
* Please include a "Receive Security Release Notifications" section at the very bottom of the blog post with links to our contact us page and RSS feed. See previous security release posts or [this issue](https://gitlab.com/gitlab-com/gl-security/security-communications/communications/issues/145#note_240450797) for examples.
* Make sure that the blog post only mentions fixes that actually made it into the release. Consider the [48h deadline before the Security Release due date](https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/security/process.md#security-release-deadlines), every issue associated (even after the deadline) will likely not be included and we should ensure it's not mentioned on the blog post. For anything that was removed, please comment on the related security issue with the blog post text that you have written.
* Assign the blog post to another member of the appsec team for review.
* Create a WIP issue using the `Security Release Email Alert` template in https://gitlab.com/gitlab-com/gl-security/security-communications/communications/issues for the communications team and request an email notification be sent to subscribers of the `Security Alert` segment on the day of the release. Include a note that this should be sent out **after** the blog post is live. Also mention that you'll include the link to the blog post MR once it is prepared. The content of this notification should be similar to the blog post introduction:

>"Today we are releasing versions X.X.X, X.X.X, and X.X.X for GitLab Community Edition (CE) and Enterprise Edition (EE).

>These versions contain a number of important security fixes, and we strongly recommend that all GitLab installations be upgraded to one of these versions immediately. 

>Please forward this alert to appropriate people at your organization and have them subscribe to [Security Notices](https://about.gitlab.com/contact/). You can also receive security release blog updates by subscribing to our [RSS feed](https://about.gitlab.com/security-releases.xml).

>For more details about this release, please visit our [blog](TODO)."

### Regular security releases

1. For each provided fix for a security issue:
    1. Request a pre-assigned CVE by following the steps in the [Requesting CVEs](#requesting-cves) section.
1. Set the previous month's regular security release to public by following the [post release steps](https://gitlab.com/gitlab-org/release/docs/blob/master/general/security/security-engineer.md#post-release) for it.
1. Create a new issue for the next scheduled security release and begin adding issues which are ready to be backported and released.  This may need to happen earlier to track fixes that did not meet the deadline. Document this new release issue in the title of the #releases Slack channel.

### Critical security releases

The issue must be prepared as soon as we are aware of the critical issue.

Once a fix has been provided for a critical `S1` labelled security issue:

1. Request a pre-assigned CVE by following the steps in the [Requesting CVEs](#requesting-cves) section.
1. Notify release managers of a need to prepare for the critical security
   release. If the fix is not ready yet, provide release managers with an
   estimate of when the fix is going to be ready so that they can create a
   release plan.
1. **30 days after** the release is published, follow steps in [Post Release](#post-release).


### Requesting CVEs

1. Visit the (gitlab-org/cves)[https://gitlab.com/gitlab-org/cves/-/issues] repository issue tracker. You will be requesting CVEs by making issues using the templates in this repository.
1. For each vulnerability (CVSS score > 0), request a CVE identifier by creating a new issue and selecting the `Internal GitLab Submission` template. Please note that some merge requests included in the security release may not contain any vulnerability fixes (e.g., only third-party dependency upgrades or security enhancements, etc.), and thus do not require a CVE identifier.
1. For the `Title` of each issue, please use the respective vulnerability title that is going to be included in the blog post entry.
1. Fill in the remaining fields with the relevant information using the template or other previous submissions as examples. Please note that you will likely need to look up the appropriate `cwe` value (see [CWE](https://cwe.mitre.org/)) and also use the [CVSS calculator](https://nvd.nist.gov/vuln-metrics/cvss/v3-calculator) to determine the `impact`.
1. Note: some information, such as `fixed_versions` and `solution`, may not be readily available when the CVE request issue is created. Be sure to update the CVE issue once you have that information.
1. Submit the issue. It will be reviewed by the vulnerability research team. They will follow up with any questions and then submit it to be assigned a CVE identifier.
1. Comment on the relevant security issue for each vulnerability submitted with a link to the CVE request issue. Once a CVE identifier is assigned, please also comment with that on the relevant issue and ensure that it gets included in the blog post.

### Verify fixes

Each GitLab security fix included in a Security Release (whether is regular or critical) should be validated and approved by an AppSec team member.

1. Engineers will request a security review from a member of the AppSec team on the merge request targeting `master`.
2. AppSec engineer will validate the security vulnerability has been remediated by:
   * Doing a code review from the security perspective and,
   * Validating the security fix using the Docker Image generated by [`package-and-qa`], see following section for more details.
3. Once AppSec engineer has confirmed the merge request fixes the security vulnerability, they will approve the merge request.

**Note: Approval is only required for the merge request targeting `master`, it's not required for the merge requests
targeting a versioned stable branch (`X-Y-stable-ee`)**

**Note: The process described above is particular to validate GitLab fixes. The process to verify fixes for other subcomponents (Omnibus GitLab, Gitaly, and GitLab Pages)
might be different**

#### Validating security fixes using `package-and-qa`

**Note: Delivery team recorded [a video](https://youtu.be/0IP3m48zWRg) about summarizing this process, it's highly encouraged for AppSec team members
to see it**

`package-and-qa` build runs end-to-end tests against an Omnibus package that is generated from the merge request changes,
to validate the security fix introduced on the merge request remediates the vulnerability, Security engineers will use this package
to spin up a docker image in their local environment.

For that, Security Engineers need to follow these steps:

1. On the security merge request, click on `qa` stage and then trigger the `package-and-qa` build.
1. Internally `package-and-qa` build will trigger a pipeline on the [Omnibus GitLab Mirror] project.
1. On the [Omnibus GitLab Mirror] pipeline, wait for the `Trigger:gitlab-docker` build to finish.
1. In the meantime,
    * Ensure you're login to `registry.gitlab.com`, you can login with your preconfigured Docker credentials,
      or with a [Personal Access Token] or a [Deploy Token].
    * Complete the [Set up volumes location] on the Omnibus
1. Once `Trigger:gitlab-docker` has been completed, scroll down to the end of the log
and find the docker image that was pushed to `registry.gitlab.com`.
1. To start the docker image on your local environment, use the following command:

```
sudo docker run --detach \
  --hostname gitlab.example.com \
  --publish 443:443 --publish 80:80 --publish 22:22 \
  --name gitlab \
  --restart always \
  --volume $GITLAB_HOME/config:/etc/gitlab \
  --volume $GITLAB_HOME/logs:/var/log/gitlab \
  --volume $GITLAB_HOME/data:/var/opt/gitlab \
  <registry image>
```

1. Wait for the installation to be completed, and after that, you'll be able to access the local instance
by going to `0.0.0.0:80` in your browser.

#### Finalize release

Once blog post on dev.gitlab.org has been reviewed and all packages are ready for
publishing:

* Release manager starts publishing the packages
* **When the packages are ready to be merged by the Release Team into master branch:** create an MR with the blog post file in https://gitlab.com/gitlab-com/www-gitlab-com.
  * Destination Directory: `source/releases/posts` by copying the blog post MR that was prepared on `dev.gitlab.org`. 
  * The file name should be in the format **`YYYY-MM-DD-security-release-gitlab-X.X.X.released.html.md`**
* Put the link of the new blog post in the email notification request issue as well as the security release issue
* Make sure the CVE IDs are documented in the corresponding GitLab.com issues and H1 reports
* Ping in H1 reports the appsec team member who triaged the issue to notify a fix has been released
* Close out the issues on both `gitlab-org/gitlab` and `gitlab-org/security/gitlab`.

#### Post release

30 days after release
 * Open a new security release issue for the next security release. Identify the next release managers from the [release managers page](https://about.gitlab.com/release-managers/) and assign the issue to them.
 * Remove `confidential` from issues unless they have been labelled `~keep confidential`.
   * Our **bot** will automatically remind us to remove `confidential` from the issues.

[summary table]: https://gitlab.com/gitlab-org/security/gitlab/-/blob/master/.gitlab/issue_templates/Security%20developer%20workflow.md#summary
[GitLab Security]: https://gitlab.com/gitlab-org/security/gitlab
[`package-and-qa`]: https://docs.gitlab.com/ee/development/testing_guide/end_to_end/#using-the-package-and-qa-job
[Omnibus GitLab Mirror]: https://gitlab.com/gitlab-org/build/omnibus-gitlab-mirror/
[Personal Access Token]: https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html
[Deploy Token]: https://docs.gitlab.com/ee/user/project/deploy_tokens/
[Set up volumes location]: https://docs.gitlab.com/omnibus/docker/#set-up-the-volumes-location
