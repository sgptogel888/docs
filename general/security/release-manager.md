# Security Releases as a Release Manager

## Overview

As noted in the [general process document](process.md), release managers
are responsible for handling [regular](#regular-security-releases) and
[critical](#critical-security-releases) security releases. Before reading
the specifics for each, please read through the general
information below which applies to both.

When working on security releases, it's important that all work happens
**only** on [Security repositories], so nothing is disclosed publicly before we intend
it.

We normally try to avoid releasing security packages to the community on Friday
in order to avoid situations where a customer may be exploited during the
weekend.

### What to include

A security release, should _only_ include the changes necessary to resolve the security vulnerabilities.
Including fixes for regressions in a security patch increases the chances of breaking
something, both for users and for our packaging and release process.

Security merge requests will be included in a Security Release if:

* The security implementation issue is associated to the Security Release Tracking Issue.
* Security merge request targeting `master` has been approved by an AppSec team member.
  * Note: This only applies for GitLab security fixes. It doesn't apply for security fixes on Omnibus, GitLab Pages or Gitaly.

The only exception to this policy is [release candidates]. If the monthly
release is in progress as we're preparing for a security release, it's
acceptable for a new RC to include both security fixes and regression fixes.
Care should be taken to coordinate the publishing of an RC package with the
other security patches so as to not disclose the security vulnerabilities
publicly before we're ready to disclose them.

## Regular Security Releases

From a release manager standpoint, a non-critical security release is fairly
similar to a [patch release], but with a few extra considerations
in order to avoid disclosing vulnerabilities before proper fixes are released.
Additional collaboration is needed to coordinate with the AppSec and Marketing
teams.

For this type of security releases, the 28th of the month is used as default due date,
see [security release deadlines] for more info.

### General steps

1. An AppSec engineer opens a Security Release Tracking Issue, this is a confidential issue in
`gitlab-org/gitlab` that serves as a high-level overview of an entire security release. 

1. After the 22nd, Release Managers will start merging security merge requests targeting `master`
with `/chatops run release merge --security --master`
   - Release tools will only consider security merge requests that
      - Are related to security issues associated to the current Security Release Tracking Issue
      - Are associated to security issues that are ready to be processed.
   - Once security merge requests have been merged, Canonical and Security `master` branches will diverge
    and the mirroring between these two repositories will stop working.
    This means Release Manager will need to update the Security `master` branch to continue with the auto-deploy processes.
    See [troubleshoot guide](#troubleshoot-guide) for detailed steps about bringing Security `master` up to date.

1. Near the due date, Release Managers will create the Security Release task issue in
[release/tasks repo](https://gitlab.com/gitlab-org/release/tasks), with `/chatops run release prepare --security`

1. It's the Release Managers responsibility to follow the steps listed on the Security Release
task issue and to complete the Security Release process.

## Critical Security Releases

Critical Security Releases are triggered on demand by an AppSec engineer. From a release manager standpoint,
a critical security release requires drafting a release plan with a timeline in addition to tasks defined for a [regular
security release](#regular-security-releases).

It is important to note that "critical" does not mean that the release needs to
happen the very moment it's requested. There might be multiple release tasks
that are currently executed, so it is important not to panic.

A few questions that you can discuss with your fellow release manager:

* Are the fixes ready for issue(s) that prompted this security release?
  * If no, find out when the fixes will be ready for the issue and *all backport* versions.
  * If yes, are all backports ready? If yes, this means that release managers have everything they need to work on the release.
* Where are we currently in the release cycle?
  * If you are working on patch releases:
    * If the release is not tagged, consider postponing the patch release.
    * If the release is tagged, complete the patch release before proceeding further.
  * If you are working on the latest monthly release:
    * Will working on security release endanger the monthly release? Is it
      possible to work on RCs and all backports for the security release without
      breaching the deadline?
    * If you prepare a security release and it gets postponed for some reason,
      will creating all other RCs as security releases cause more work?

When you get answers for the questions above, start working on the timeline for
the critical security release.

An example of how a release timeline could look like, when a security release
is called for during the latest monthly release:

```
To meet the deadline of 22nd and not block the regular release, proposing a schedule:

* All fixes and backports need to be ready for merge by the end of Monday.
* On Tuesday, @release-manager-1 will merge all security fixes into their respective branches,
  and deploy the security fixes to our different environments.
* On Tuesday, @release-manager-2 will ensure the deployment makes it to production and tag the release.
* On Wednesday, @release-manager-1 will publish the packages and wrap up the rest of the security release tasks.
```

The plan can be more detailed to include more people with specific tasks.
Aim to `@`-mention people responsible for specific tasks, to avoid the
[bystander effect](https://en.wikipedia.org/wiki/Bystander_effect).

Once the plan is finalized it is important to stick with the plan. You should refuse any changes
with the items that need to be included in the release if they put the release deadlines at risk.
You can consider being flexible if there is enough time to recover from failure
(CI failing, deploy goes wrong, etc.), but in most cases you will need to stick strictly to the
plan to make sure that the deadlines given to the Security team are respected.

Once the plan is defined, Release Managers can create the release tasks issue with
`/chatops run release prepare --security --critical` and follow the steps listed there.

## Troubleshoot guide

### How do I update Security `master` with the content of Canonical `master`? 

Security `master` is automatically mirrored based on the content of Canonical `master`,
that means that every commit push into Canonical `master` is automatically pushed to Security `master`.

Once security merge requests targeting master start being merged in Security `master`,
content of Canonical and Security master will diverge (security `master` will contain the security fixes),
this will halt the automatic mirroring between these two.

A [scheduled
task](https://ops.gitlab.net/gitlab-org/release/tools/-/pipeline_schedules/121/edit)
in release-tools will check for this divergence periodically and automatically
enable the [security merge-train pipeline schedule] to regularly update the
Security `master` branch until the divergence is resolved.

If you need to force an execution of the merge-train outside of this schedule,
you can:

* Go to the [security merge-train pipeline schedule].
* Click play.

This will trigger a merge-train that will merge Canonical `master` into Security
`master`. Full execution of merge-train takes approximately 6 minutes.

Considering that auto-deploy branches are created solely on Security every 6 hours, it's advised for Release Managers
to trigger the merge-train frequently.

### What to do if merge-train fails to update Security `master`?

There's a chance of a file being modified both on Canonical and on Security,
if this happens merge-train will fail when performing the merge between the two repositories.

The failure will generate a notification on the `#g_delivery` Slack channel.
Solving this will require coordination by the Release Managers:

* Analyze how difficult it's to recover from breakage.
  * If the conflict is easy to solved, fix it and push the changes to Security `master`
  * If the conflict is more involved, ping the developers related to the changes, if they're not available, use the dev-on-call process.
* If the conflict blocks deployments, follow the [deployment blockers process] and raise an incident.

### What to do if the new branch is identical to the previous one

Sometimes we see cases of a new auto-deploy branche being identical to the previous one. To confirm:

* Find the latest branch in `#releases` Slack channel
* Compare the branch contents to the previous branch
* If they're the same you need to:
   * Trigger the security merge-train
   * Manually merge the content of Security `master` into the current auto-deploy branch

Issue https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1142

---

[Return to Security Guide](process.md)

[release candidates]: ../release-candidates.md
[Security repositories]: https://gitlab.com/gitlab-org/security/
[patch release]: https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/patch/process.md
[security release deadlines]: https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/security/process.md#security-release-deadlines
[Release Managers]: https://about.gitlab.com/community/release-managers/
[automatic toggling of merge-train]: https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1111
[deployment blockers process]: https://about.gitlab.com/handbook/engineering/releases/#deployment-blockers
[security merge-train pipeline schedule]: https://ops.gitlab.net/gitlab-org/merge-train/-/pipeline_schedules?scope=inactive
