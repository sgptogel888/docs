# Deployer CI variables

Our tooling makes heavy use of environment variables for functionality. This
document aims to provide a reference for the most important ones, but is not
necessarily comprehensive.

Please note that items that are specific to configurations or are set by CI jobs
may not be documented below.  For these, refer to the `gitlab-ci.yaml` or the
CI/CD Variables for that project.

## Change-lock

https://gitlab.com/gitlab-com/gl-infra/change-lock

| Variable Name                  | Purpose         |
| ------------                   | ------------    |
| `CHANGE_LOCK_FORCE_FAILURE`    | Forces change-lock to halt immediately, this will exit the job and report a job failure to CI |
| `CHANGE_LOCK_OVERRIDE`         | Overwrites the configuration when pipelines attempt to run during times which a change lock would normally fail a pipeline |

## Deployer

https://ops.gitlab.net/gitlab-com/gl-infra/deployer

| Variable Name             | Purpose         |
| ------------              | ------------    |
| `ALLOW_K8S_FAILURE`       | Instructs Ansible to skip reporting a failure if the triggered Kubernetes pipeline fails |
| `ALLOW_TRACK_FAILURE`     | When set to `true`, allows deployment tracking jobs to fail. Be sure to create an issue in [gl-infra/delivery](https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues) when there is a tracking failure, and in that issue mention this override is set so we don't forget to unset it. |
| `CHECKMODE`               | Forces ansible to run in dry-run |
| `CMD`                     | Instructs ansible to run specific commands, see [one-off command](https://ops.gitlab.net/gitlab-com/gl-infra/deploy-tooling/-/tree/master/cmds) documentation |
| `CNY_MANUAL_PROMOTE`      | Adds a manual promotion job to the Deployment Pipeline |
| `SKIP_WAIT_FOR_PID_CHANGE`| Instructs Ansible to not wait for PID changes during rolling deployments/restarts of services |
| `DEPLOY_ENVIRONMENT`      | The Environment(s) to which Ansible will attempt to perform a deployment or run a custom action |
| `DEPLOY_ROLLBACK`         | Utilized during times when one must rollback a production deployment.  This essentially reverses a few jobs in the pipeline to minimize the potential for user surfacing errors in the live environment. |
| `DEPLOY_VERSION`          | Instructs Ansible which version of GitLab is to be installed |
| `GITLAB_INSTALL_OVERRIDE` | Always install the omnibus package, even if the version hasn't changed. |
| `IGNORE_PRODUCTION_CHECKS`| Allows a production deploy to continue despite any warnings detected by the release-tools auto_deploy:check_production failing any checks |
| `PRECHECK_IGNORE_ERRORS`  | Instructs Ansible to ignore failures in our `prechecks_pipeline` play |
| `ANSIBLE_SKIP_TAGS`       | Skip certain tasks during deploy, setting this to `haproxy` will skip all haproxy operations (removing, adding nodes, etc.) and is the same as passing `--skip-haproxy` to the deploy chatops command. |
| `SKIP_OMNIBUS_ROLE_CHECK` | Bypass the check that will not disable omnibus version updates, if they are already disabled |

### K8S-Workloads

https://ops.gitlab.net/gitlab-com/gl-infra/k8s-workloads/gitlab-com

| Variable Name                  | Purpose         |
| ------------                   | ------------    |
| `SKIP_QA`                      | Instructs `.gitlab-ci.yml` to skip running a QA job after the deploy has completed |

:warning: **The above lists are not exhaustive.  Please refer to the
.gitlab-ci.yml file for any items that are set by our own CI jobs**

# Release Tools CI Variables

Many variables are associated with our release-tools.  This list can be found
here: https://gitlab.com/gitlab-org/release-tools/-/blob/master/doc/variables.md

---

[Return to Documentation](../README.md#documentation)
